require "sinatra"
require "faraday"

NETWORK_SERVER = {
  ircnet: "irc.snt.utwente.nl",
  tweakers: "irc.tweakers.net",
}

enable :sessions

get '/' do
  haml :index
end

post '/link' do
  begin
    session.update(params)
    conn = Faraday.new do |f|
      f.request :json
      f.response :logger
      f.response :json, content_type: []
    end
    res = conn.post "https://matrix-irc.snt.utwente.nl/#{params[:network]}/provision/link",
              remote_room_server: NETWORK_SERVER[params[:network].to_sym],
              remote_room_channel: params[:channel],
              matrix_room_id: params[:room_id],
              op_nick: params[:op_nick],
              user_id: params[:user_id]

    pp res.body
    if res.success?
      session[:notice] = "Link request posted"
    else
      session[:alert] = res.body["error"]
    end
  rescue Faraday::TimeoutError
    session[:alert] = "Timeout waiting for response"
  rescue => e
    session[:alert] = e.message
  end

  redirect back
end
